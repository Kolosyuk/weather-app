# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Simple weather web-app project for test.
Uses: 
* typescript
* react
* redux-toolkit
* redux-persist
* translating library - i18next
* chartjs
* OpenWeatherMapAPI

UI - [link](https://drive.google.com/file/d/1jzaWw-iUVXO4Wwbf0Mu1Q9Z2-8fUT78u/view?usp=sharing)

[link to Netlify ](https://weather-app-kolosyuks.netlify.app/)