export enum Modes {
  METRIC = 'metric',
  IMPERIAL = 'imperial',
};

export interface IState {
  cities: ICities;
  settings: ISettings;
};

export interface ICities {
  cities: IStoredCity[];
  tempCities:ICityAPI[];
  error: null | string;
};

export interface ISettings {
  lang: string;
};

interface IWind {
  speed: number;
  deg: number;
};

interface ISys {
  type: number;
  id: number;
  country: string;
  sunrise: number;
  sunset: number;
};

interface IClouds {
  all: number;
};

interface IMain {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
};

export interface IWeatherAPI {
  id: number;
  main: string;
  description: string;
  icon: string;
};

interface ICoord {
  lon: number;
  lat: number;
};

export interface IStoredCity {
  id: number;
  mode: string;
  name: string;
  loading: boolean;
  latitude?: number | string;
  longitude?: number | string;
};

export interface ICard {
  id: number;
  mode: string;
  name: string;
  loading: boolean;
  latitude?: number | string;
  longitude?: number | string;
};

export interface ICityAPI extends ICard {
  timezone?: number;
  cod?: number;
  sys?: ISys;
  dt?: number;
  clouds?: IClouds;
  wind?: IWind;
  visibility?: number;
  main?: IMain;
  base?: string;
  weather?: IWeatherAPI[];
  coord?: ICoord;
  // mode: string;
};