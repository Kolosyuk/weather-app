import { configureStore, combineReducers } from "@reduxjs/toolkit";
import citiesReducer from './redusers/citiesSlice';
import settingsReducer from './redusers/settingsSlice';
import { useDispatch } from 'react-redux';
import { 
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'weather-app',
  storage,
  blacklist: ['cities']
};

const persistCityConfig = {
  key: 'cities',
  storage,
  blacklist: ['tempCities']
};

const rootReducer = combineReducers({
  cities: persistReducer(persistCityConfig, citiesReducer),
  settings: settingsReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer)


const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store)

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch

export default store;
