import { createSlice } from '@reduxjs/toolkit';
import { ISettings } from '../../types/types';

const initialState: ISettings = {
  lang: 'en'
};

const settingsSlice = createSlice({
  name: 'settings',
  initialState: initialState,
  reducers: {
    changeLanguage(state, action){
      state.lang=action.payload.value
    },
  },
});

export const { changeLanguage } = settingsSlice.actions;

export default settingsSlice.reducer;