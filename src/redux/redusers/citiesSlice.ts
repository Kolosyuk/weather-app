import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { ICities, IStoredCity, ICityAPI } from '../../types/types';
import { searchString } from '../../helpers';

const initialState: ICities = {
  cities: [],
  tempCities: [],
  error: null,
}

export const fetchWeather = createAsyncThunk(
  'cities/fetchWeather',
  async function(city : IStoredCity, {rejectWithValue}){
    try {
      const response = await fetch(searchString(city));

      if (!response.ok) {
        throw new Error('Fetch error');
      } 

      const data = await response.json();
      return {
        ...data,
        id: city.id,
        mode: city.mode
      };
    } catch (error) {
      return rejectWithValue(city.name);
    }
  }
);

//RTK оборачивает в Proxy, "мутируем" стейт напрямую

const citiesSlice = createSlice({
  name: 'cities',
  initialState: initialState,
  reducers: {
    addCity(state, action : PayloadAction<IStoredCity>){

      const isExist = state.cities.find(city => city.name.toLocaleUpperCase() === action.payload.name.toLocaleUpperCase())

      if (!isExist) {
        state.cities.push(action.payload);
      } 
    },
    deleteCity(state, action : PayloadAction<number>){
      state.cities = state.cities.filter((item) => item.id !== action.payload)
      if(state.tempCities){
        state.tempCities = state.tempCities.filter((item) => item.id !== action.payload)
      }
    },
    updateCityMode(state, action : PayloadAction<IStoredCity>){
      state.cities = state.cities.map(city => city.id !==action.payload.id 
        ? city 
        : {
          ...city, mode: action.payload.mode
        }
        );
    },
    resetCities(state, action : PayloadAction<number>) {
      state.cities = state.cities.map(city => city.id !==action.payload 
        ? city 
        : {
          ...city, loading: true
        }
        );
    }
  },
  extraReducers: {
      'cities/fetchWeather/pending' : (state : ICities) => {
        state.error = null;
      },
      'cities/fetchWeather/fulfilled' : (state : ICities, action : PayloadAction<ICityAPI>) => {
        if (state.tempCities) {
          state.tempCities.push(action.payload);
          state.cities = state.cities.map(city => city.id !==action.payload.id 
            ? city 
            : {
              ...city, loading: false
            }
            );
        }
      },
      'cities/fetchWeather/rejected' : (state : ICities, action : PayloadAction<string>) => {
        state.cities = state.cities.filter((item) => item.name !== action.payload)
      }
  },
});

export const { 
  addCity,
  deleteCity,
  updateCityMode,
  resetCities,
} = citiesSlice.actions;

export default citiesSlice.reducer;