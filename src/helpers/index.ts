import { IStoredCity, Modes } from '../types/types';
const API_BASE = 'https://api.openweathermap.org/data/2.5/weather?';
const API_FORECAST_BASE = 'https://api.openweathermap.org/data/2.5/forecast';
const API_KEY = '0b5016dcb83396b6dec20f1d7801e0f0'; 

export function searchString({name, mode, id, longitude, latitude} : IStoredCity) {

  console.log(longitude);
  
  if (longitude) {
    return `${API_BASE}lat=${latitude}&lon=${longitude}&appid=${API_KEY}&units=${mode}`
  }
  
  return `${API_BASE}q=${name}&APPID=${API_KEY}&units=${mode}`
};

export function getForecastURL(name : string, mode : string) {
  return `${API_FORECAST_BASE}?q=%20${name}&appid=82419b3c788ed1dd4419d635e8cf7c97&units=${mode}`
};

export function getIconUrl(iconCode: string = '') {
  return `http://openweathermap.org/img/wn/${iconCode}@2x.png`;
};

export function truncToWholeNumber (num: number = 0, trim: number = 0) {
  return +num.toFixed(trim);
};

export function coldOrHot(num: number = 0, mode : string) {
  if (mode === Modes.METRIC) {
    if (num > 0) return `+${num}`;
    return `${num}`;
  } else {
    if (num > 32) return `+${num}`;      
    return `${num}`;
  };
};

export function selectDatesForChart(item : any, ind : number) {
  const date =new Date(item.dt*1000).getDate();
  const midDay = new Date(item.dt*1000).getHours() === 12;
  
  if (ind === 0 || midDay) {
    return date
  };
};

export function selectTempForChart(item : any, ind : number ) {
  
  const isMidDay = new Date(item.dt*1000).getHours() === 12;
  
  if ((ind === 0) || (isMidDay)) {
    return +(item.main.temp).toFixed()
  };
}

export function celciusToFarenheit(temp:number = 0) {
  return +((temp*1.8)+32).toFixed(); 
};

export function farenheitToCelcius(temp:number = 0) {
  return +((temp-32)/1.8).toFixed(); 
};

export function createUniqueId() {
  return new Date().valueOf();
};


export function getCardClasses(temp : string, mode : string) {
  if (mode === Modes.METRIC) {
    return (temp[0] !== '-') && (parseInt(temp[0]) !== 0) ? "card card--warm" : "card card--cold";
  } ;
  if (parseInt(temp) > 32) {
    return "card card--warm";
  } else {
    return "card card--cold";
  };
};

export function getWHPClasses(temp : string, mode : string) {
  if (mode === Modes.METRIC) {
    return temp[0] !== '-' ? "warm" : "cold";
  } ;
  if (parseInt(temp) > 32) {
    return "warm";
  } else {
    return "cold";
  };
};