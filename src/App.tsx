import { useEffect } from 'react';
import { useTypedSelector } from './hooks/useTypedSelector';
import { useAppDispatch } from './redux/store';
import { addCity } from './redux/redusers/citiesSlice';
import { Modes } from './types/types';
import { Header } from './components/header/header';
import { CardTable } from './components/card-table/card-table';
import { createUniqueId } from './helpers';

import i18n from './i18';

import './App.css';

function App() {

  const { lang } = useTypedSelector(state => state.settings);
  const dispatch = useAppDispatch();

  useEffect(() => {
    i18n.changeLanguage(lang);
  }, [lang]);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(function(position) {

      console.log(position.coords.latitude);
      console.log(position.coords.longitude);
      

      dispatch(addCity({
        name: 'Your location', 
        mode : Modes.METRIC, 
        id: createUniqueId(),
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        loading: true,
      }));
    }, ((err) => console.log('location error', err)), { enableHighAccuracy: true, timeout: 20000, maximumAge: 0 });
  }, [dispatch]);


  return (
    <div className="App">
      <Header />
      <CardTable />
    </div>
  );
}

export default App;
