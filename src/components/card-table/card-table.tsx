import { FC } from 'react';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { IStoredCity } from '../../types/types';
import CardWithData from '../card-with-data/card-with-data';

import './card-table.css';

export const CardTable : FC = () => {

  const { cities, error } = useTypedSelector(state => state.cities);

  if(error) {
    <h1> {error} </h1>
  };

  return (
    <div className='card-table'>
      <div className='container'>
        <ul className='card-list'>
        {
          cities.map((city : IStoredCity) => {
            return <CardWithData 
                    key={city.id}
                    id={city.id}
                    name={city.name} 
                    mode={city.mode}
                    loading={city.loading}
                    latitude={city.latitude}
                    longitude={city.longitude}
                    />
          })
        }
        </ul>
      </div>
   </div>
  )
};