import { useState, FC} from 'react';
import { useAppDispatch } from '../../redux/store';
import { useTranslation } from 'react-i18next';
import { LangChanger } from '../lang-changer/lang-changer';
import { addCity } from '../../redux/redusers/citiesSlice';
import { Modes } from '../../types/types';
import { createUniqueId } from '../../helpers';

import './header.css';

export const Header: FC = () => {

  const [name, setNewName] = useState<string>('');
  const dispatch = useAppDispatch(); 
  const { t } = useTranslation();

  const handleChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNewName(e.target.value);  
  };

  const handleAddCity = (e : React.MouseEvent<HTMLButtonElement>) => {
    dispatch(addCity({name, mode : Modes.METRIC, id: createUniqueId(), loading: true}));
    setNewName('');
  };

  return (
    <div className='header'>

      <div className='header body'>
       <input
            className='header__input'
            type="text" 
            name="name" 
            value={name} 
            onChange = {handleChangeName} 
            placeholder="Enter city" 
          />
          
              <button 
                className='btn'
                onClick ={handleAddCity}
                disabled={!name} >
              {t('add city')}
              </button>
      </div> 
      <LangChanger />
    </div>
  )
};
