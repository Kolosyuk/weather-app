import { FC, useEffect } from 'react';
import { useAppDispatch,  } from '../../redux/store';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { fetchWeather, resetCities } from '../../redux/redusers/citiesSlice';
import { IStoredCity, ICityAPI, ICard } from '../../types/types';
import Card from '../card/card';
import Spinner from '../spinner/spinner';

import './card-with-data.css';

const CardWithData : FC<ICard> = ({ name, id, mode, loading, latitude, longitude }) => {

  const dispatch = useAppDispatch();
  const { tempCities } = useTypedSelector(state => state.cities);  

  const city = tempCities.find(item => item.id === id);


  useEffect(() => {
      dispatch(resetCities(id));
      dispatch(fetchWeather({name, mode, id, latitude, longitude} as IStoredCity));
      // eslint-disable-next-line
    }, []);

    return (
      <li key={id} className='card-item'>
        {
          loading && <Spinner />
        }
        {
          !loading && <Card {...city as ICityAPI} mode={mode}/>
        }
      </li>
    )

}

export default CardWithData;