import { FC } from 'react';
import Select, { SingleValue } from 'react-select';
import { useTranslation } from 'react-i18next';
import { useAppDispatch } from '../../redux/store';
import { changeLanguage } from '../../redux/redusers/settingsSlice';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { useTypedSelector } from '../../hooks/useTypedSelector';

import './lang-changer.css';

type Value = {
  label: string;
  value: string
}

export const LangChanger : FC  = () => {

  const { i18n,  } = useTranslation();
  const dispatch = useAppDispatch();
  const { lang } = useTypedSelector(state => state.settings);

  const changeLanguageHandler = (value : SingleValue<Value>) => {
    i18n.changeLanguage(value?.value);
    dispatch(changeLanguage(value));
  };

  const selectStyles = {
    control: (base : any) => ({
      ...base,
      width: '110px',
      fontSize: '16px',
      fontWeight: 'bold',
      borderRadius: '8px',
      padding: '6px 5px',
      border: 'none',
      boxShadow: 'none',
      '&:focus': {
          border: '0 !important',
      },
    }),
  };

  const options = [
      {label: 'EN', value: 'en'},
      {label: 'RU', value: 'ru'},
      {label: 'UA', value: 'ua'},
  ];  
  
  return (
    <div className='lang-switcher'>
      <FontAwesomeIcon icon={solid('globe')} />
      
      <Select 
      onChange={(value : SingleValue<Value>) => {
        changeLanguageHandler(value as SingleValue<Value>)
      }}
      options={options}
      styles={selectStyles}
      value={options.filter(option => option.value === lang)[0]}
      />
    </div>
  )
};