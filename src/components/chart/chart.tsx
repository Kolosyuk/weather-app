import { FC, useEffect, useState } from 'react';
import { Modes } from '../../types/types';
import { Line } from 'react-chartjs-2';
import type { ChartData } from 'chart.js';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Filler,
} from 'chart.js';
import { 
  selectDatesForChart,
  selectTempForChart,
  getForecastURL
} from '../../helpers';

interface IChartProps {
  name: string;
  mode: string;
}

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Filler,
);

export const options = {
  responsive: true,
  bezierCurve: false,
  lineTension: 1,
  scales: {
    x: {
      grid: {
        display: false
      }
    },
    y: {
      grid: {
        display: false
      }
    }
  },  
  maintainAspectRatio: true,
};

const Chart : FC<IChartProps> = ({ name, mode }) => {

  const [ forecast, setForecast ] = useState<ChartData<'line'>>();

  useEffect(() => {
    if (!name) return;
    
    fetch(getForecastURL(name, mode))
    .then((response) => response.json())
    .then((data) => {
      const tempNow = (data.list[0].main.temp).toFixed();
      let chartColor = '#ffa25b';
      if ( (tempNow <= 0 && mode === Modes.METRIC) || (tempNow <= 32 && mode === Modes.IMPERIAL)) {
        chartColor = "#459de9";
      };

      setForecast({
        labels: data.list.map(selectDatesForChart).filter((item : number | undefined) => typeof item === 'number'),
        datasets: [
          {
            label: "temp",
            data: data.list.map(selectTempForChart).filter((item : number | undefined) => typeof item === 'number'),
            showLine: true,
            fill: true,
            borderColor: chartColor,
            backgroundColor: chartColor,
          }
        ]
      });
    })
    .catch((err) => {
      console.log(err);
    })
  }, [name, mode])

  return (
    <>
      {
        forecast && <Line options={options} data={forecast} />
      }
    </>
  )
};

export default Chart;