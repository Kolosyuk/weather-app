import { FC } from 'react';
import { Modes, IStoredCity } from '../../types/types';

import './degree-changer.css';

interface IDegreeChangerProps {
  name: string,
  id: number;
  mode: string;
  onClick: (obj: IStoredCity) => void
};

const DegreeChanger : FC<IDegreeChangerProps> = ({ name, id, mode, onClick}) => {

  const isMetric = mode === Modes.METRIC; 
  const signOrder = isMetric ? 'direct' : 'reverse';
  const DegreeChangerClasses = `temp-real_sign ${signOrder}`;
  const CelciusSignClasses = `celcius-sign ${isMetric? 'celcius-sign--active' : ''}`;
  const FarenheitSignClasses = `farenheit-sign ${isMetric? '' : 'farenheit-sign--active'}`;

  return (
    <div className={DegreeChangerClasses}>
      <span className={CelciusSignClasses} onClick={() => {
        onClick({name, id, mode: Modes.METRIC, loading: false})
      }}>{'\u2103'}</span> 
      | 
      <span className={FarenheitSignClasses} onClick={() => {
        onClick({name, id, mode: Modes.IMPERIAL, loading: false})
      }}>{'\u2109'}</span>
    </div>
  )
};

export default DegreeChanger;