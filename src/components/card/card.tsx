import { FC, useEffect, useState } from 'react';
import { deleteCity, updateCityMode } from '../../redux/redusers/citiesSlice';
import { useAppDispatch,  } from '../../redux/store';
import { format } from 'date-fns';
import { enGB, ru, uk } from 'date-fns/locale';
import { IStoredCity, Modes, ICityAPI } from '../../types/types';
import Chart from '../chart/chart';
import { useTranslation } from 'react-i18next';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DegreeChanger from '../degree-changer/degree-changer';
import { 
  getIconUrl,
  truncToWholeNumber,
  coldOrHot,
  celciusToFarenheit,
  farenheitToCelcius,
  getCardClasses,
  getWHPClasses
} from '../../helpers';

import './card.css';

interface ILocales {
  [key: string]: Locale;
}

const locales : ILocales = {
  'en': enGB,
  'ru': ru,
  'ua': uk
}

const Card : FC<ICityAPI> = ({ id, name, mode, main, weather, wind, sys, dt }  ) => {

  const { t, i18n } = useTranslation();
  const dispatch = useAppDispatch();
  const [ realTemp, setRealTemp] = useState<string>('');
  const [ feelTemp, setFeelTemp] = useState<string>('');

  useEffect(() => {
    setRealTemp(coldOrHot(truncToWholeNumber(main?.temp), mode));
    setFeelTemp(coldOrHot(truncToWholeNumber(main?.feels_like), mode))
    // eslint-disable-next-line
  }, [main?.temp]);

  const handleRemoveCity = (e : React.MouseEvent) => {
    dispatch(deleteCity(id));
  };

  const handleUpdateCity = (obj : IStoredCity) => {
    if (obj.mode === Modes.IMPERIAL) {
      setRealTemp(normalTempView(realTemp, Modes.IMPERIAL));
      setFeelTemp(normalTempView(feelTemp, Modes.IMPERIAL));
    } else {
      setRealTemp(normalTempView(realTemp, Modes.METRIC));
      setFeelTemp(normalTempView(feelTemp, Modes.METRIC));
    };
    dispatch(updateCityMode(obj));
  };

  function normalTempView(temp: string, mode : string) {
    if (mode === Modes.METRIC){
      return coldOrHot(farenheitToCelcius(parseInt(temp, 10)), mode);
    } else {
      return coldOrHot(celciusToFarenheit(parseInt(temp, 10)), mode);
    }
  };

  let description: string = weather ? weather![0].main : '';
  let iconPath : string =  weather ? getIconUrl(weather![0].icon) : '';
  let time_stampt: number = dt ? dt : 0;

  const units = mode === Modes.METRIC ? '\u2103' : '\u2109';
  const windSpeed = truncToWholeNumber(wind?.speed, 1);
  const cardClasses = getCardClasses(realTemp, mode); 
  const WHPClasses = getWHPClasses(realTemp, mode);
 
  const date = format(new Date(time_stampt * 1000), 'EEE, d MMMM, HH:mm', {
    locale: locales[i18n.language]
  });
 
  return (
      <div className={cardClasses}>
        <div className='card-header'>
          <div className='card-header__left'>
            <p className='card-header__city'>{name}, {sys?.country}</p>
            <p className='card-header__date'>{date}</p>
          </div>
          <div className='card-header__right'>
            <div className='xmark-icon-container'>
              <FontAwesomeIcon className='delete-button' icon={solid('xmark')} onClick={handleRemoveCity}/>
            </div>
            <div className='weather-description'>
              <div className='weather-icon-container'>
                <img src={iconPath} alt='weather icon'/>
              </div>
              <p>{description}</p>
            </div>
          </div>
        </div>
        <div className='card-chart'>
          {
            <Chart name={name} mode={mode}/>
          }
        </div>
        <div className='card-footer'>
          <div className='card-footer__left'>
            <div className='temp-real'>
              <div className='temp-real_value'>{realTemp}</div>
              <DegreeChanger name={name} id={id} mode={mode} onClick={handleUpdateCity}></DegreeChanger>
            </div>  
            <div className='temp-feel'>{t('feels like')}: {feelTemp} {units}</div>
          </div>
          <div className='card-footer__right'>
            <p>{t('wind')}: <span className={WHPClasses}>{windSpeed} {t('m/s')}</span></p>
            <p>{t('humidity')}: <span className={WHPClasses}>{main?.humidity}%</span></p>
            <p>{t('pressure')}: <span className={WHPClasses}>{main?.pressure} {t('pa')}</span></p>
          </div>
        </div>
      </div>
  )
};

export default Card;