import i18n from "i18next";
import { initReactI18next } from "react-i18next";

const resources = {
  en: {
    translation: {
      "add city": "add",
      "wind": "wind",
      "m/s": "m/s",
      "humidity": "humidity",
      "pressure": "pressure",
      "pa": "pa",
      "feels like": "feels like"
    }
  },
  ru: {
    translation: {
      "add city": "добавить",
      "wind": "ветер",
      "m/s": "м/с",
      "humidity": "влажность",
      "pressure": "давление",
      "pa": "па",
      "feels like": "ощущается как"
    }
  },
  ua: {
    translation: {
      "add city": "додати",
      "wind": "вітер",
      "m/s": "м/с",
      "humidity": "вологість",
      "pressure": "тиск",
      "pa": "па",
      "feels like": "відчувається як"
    }
  },
};

i18n
  .use(initReactI18next) 
  .init({
    resources,
    lng: "en", 

    interpolation: {
      escapeValue: false 
    }
  });

  export default i18n;